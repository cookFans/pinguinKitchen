USE [master]
GO
/****** Object:  Database [pinguinkitchen]    Script Date: 07.03.2018 20:13:26 ******/
CREATE DATABASE [pinguinkitchen]
GO
ALTER DATABASE [pinguinkitchen] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [pinguinkitchen].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [pinguinkitchen] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [pinguinkitchen] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [pinguinkitchen] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [pinguinkitchen] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [pinguinkitchen] SET ARITHABORT OFF 
GO
ALTER DATABASE [pinguinkitchen] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [pinguinkitchen] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [pinguinkitchen] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [pinguinkitchen] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [pinguinkitchen] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [pinguinkitchen] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [pinguinkitchen] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [pinguinkitchen] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [pinguinkitchen] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [pinguinkitchen] SET  DISABLE_BROKER 
GO
ALTER DATABASE [pinguinkitchen] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [pinguinkitchen] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [pinguinkitchen] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [pinguinkitchen] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [pinguinkitchen] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [pinguinkitchen] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [pinguinkitchen] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [pinguinkitchen] SET RECOVERY FULL 
GO
ALTER DATABASE [pinguinkitchen] SET  MULTI_USER 
GO
ALTER DATABASE [pinguinkitchen] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [pinguinkitchen] SET DB_CHAINING OFF 
GO
ALTER DATABASE [pinguinkitchen] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [pinguinkitchen] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [pinguinkitchen] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [pinguinkitchen] SET QUERY_STORE = OFF
GO
USE [pinguinkitchen]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [pinguinkitchen]
GO
/****** Object:  Table [dbo].[Ingredients]    Script Date: 07.03.2018 20:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ingredients](
	[IngredientId] [int] NOT NULL,
	[IngredientDescription] [varchar](max) NULL,
	[IngredientName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Ingredients] PRIMARY KEY CLUSTERED 
(
	[IngredientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProcessingStep]    Script Date: 07.03.2018 20:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProcessingStep](
	[recipeId] [int] NOT NULL,
	[stepid] [int] NOT NULL,
	[StepDescription] [varchar](max) NOT NULL,
	[StepImage] [varchar](max) NULL,
 CONSTRAINT [PK_ProcessingStep] PRIMARY KEY CLUSTERED 
(
	[stepid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Recipe]    Script Date: 07.03.2018 20:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recipe](
	[RecipeId] [int] NOT NULL,
	[RecipeDescription] [varchar](max) NULL,
	[RecipeName] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Recipe] PRIMARY KEY CLUSTERED 
(
	[RecipeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RecipeImage]    Script Date: 07.03.2018 20:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeImage](
	[ImageId] [int] NOT NULL,
	[Image] [varchar](max) NULL,
	[recipeid] [int] NOT NULL,
 CONSTRAINT [PK_RecipeImage] PRIMARY KEY CLUSTERED 
(
	[ImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Relationship_RecipeAndIngredients]    Script Date: 07.03.2018 20:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Relationship_RecipeAndIngredients](
	[recipeid] [int] NOT NULL,
	[ingredientid] [int] NOT NULL,
	[menge] [float] NULL,
	[unit] [varchar](max) NULL,
	[id] [int] NOT NULL,
 CONSTRAINT [PK_Relationship_RecipeAndIngredients] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[ProcessingStep]  WITH CHECK ADD  CONSTRAINT [FK_ProcessingStep_Recipe] FOREIGN KEY([recipeId])
REFERENCES [dbo].[Recipe] ([RecipeId])
GO
ALTER TABLE [dbo].[ProcessingStep] CHECK CONSTRAINT [FK_ProcessingStep_Recipe]
GO
ALTER TABLE [dbo].[RecipeImage]  WITH CHECK ADD  CONSTRAINT [FK_RecipeImage_Recipe] FOREIGN KEY([recipeid])
REFERENCES [dbo].[Recipe] ([RecipeId])
GO
ALTER TABLE [dbo].[RecipeImage] CHECK CONSTRAINT [FK_RecipeImage_Recipe]
GO
ALTER TABLE [dbo].[Relationship_RecipeAndIngredients]  WITH CHECK ADD  CONSTRAINT [FK_RelationshipBRecipeAndIngredients_Ingredients] FOREIGN KEY([ingredientid])
REFERENCES [dbo].[Ingredients] ([IngredientId])
GO
ALTER TABLE [dbo].[Relationship_RecipeAndIngredients] CHECK CONSTRAINT [FK_RelationshipBRecipeAndIngredients_Ingredients]
GO
ALTER TABLE [dbo].[Relationship_RecipeAndIngredients]  WITH CHECK ADD  CONSTRAINT [FK_RelationshipBRecipeAndIngredients_Recipe1] FOREIGN KEY([recipeid])
REFERENCES [dbo].[Recipe] ([RecipeId])
GO
ALTER TABLE [dbo].[Relationship_RecipeAndIngredients] CHECK CONSTRAINT [FK_RelationshipBRecipeAndIngredients_Recipe1]
GO
USE [master]
GO
ALTER DATABASE [pinguinkitchen] SET  READ_WRITE 
GO
