using System;
using Xunit;
using System.Linq;
using PinguinKitchen.Model;

namespace pinguinKitchen.ModelTest
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal("Hello","Hello");
        }
        [Fact]
       public void Test2()
        {
            var dbContent = new pinguinkitchenContext();
            Recipe recipe = new Recipe();
  
            recipe.RecipeName = "Feuertopf";
            recipe.RecipeDescription = "Feuertopf ist ein superleckeres Gericht! Aus Guizhou kommen die besten!";
            
        }
        [Fact]
        public void UploadDownloadTest()
        {
            var dbContext = new PinguinKitchen.Model.pinguinkitchenContext();

            var recipe1 = new PinguinKitchen.Model.Recipe();

            recipe1.RecipeId = 10;
            recipe1.RecipeName = "Feuertopf";
            recipe1.RecipeDescription = "Feuertopf ist ein superleckeres Gericht! Aus Guizhou kommen die besten!";

            dbContext.Recipe.Add(recipe1);
            int numberOfChanges = dbContext.SaveChanges();

            Assert.Equal(numberOfChanges,1); // just one entity should be updated

            var dbContext2 = new PinguinKitchen.Model.pinguinkitchenContext();

            var recipe2 = dbContext2.Recipe.Where(x => x.RecipeName.Equals(recipe1.RecipeName));

            Assert.Equal(1,recipe2.Count()); // db should not have more than one

            Assert.Equal(recipe1.RecipeId,recipe2.First().RecipeId);

            dbContext2.Recipe.Remove(recipe2.First());
            dbContext2.SaveChanges();

        }
    }
}
