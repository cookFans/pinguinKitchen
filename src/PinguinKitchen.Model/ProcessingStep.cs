﻿using System;
using System.Collections.Generic;

namespace PinguinKitchen.Model
{
    public partial class ProcessingStep
    {
        public int RecipeId { get; set; }
        public int Stepid { get; set; }
        public string StepDescription { get; set; }
        public string StepImage { get; set; }

        public Recipe Recipe { get; set; }
    }
}
