﻿using System;
using System.Collections.Generic;

namespace PinguinKitchen.Model
{
    public partial class Ingredients
    {
        public Ingredients()
        {
            RelationshipRecipeAndIngredients = new HashSet<RelationshipRecipeAndIngredients>();
        }

        public int IngredientId { get; set; }
        public string IngredientDescription { get; set; }
        public string IngredientName { get; set; }

        public ICollection<RelationshipRecipeAndIngredients> RelationshipRecipeAndIngredients { get; set; }
    }
}
