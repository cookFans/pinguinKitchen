using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PinguinKitchen.Model
{
    public partial class pinguinkitchenContext : DbContext
    {
        public virtual DbSet<Ingredients> Ingredients { get; set; }
        public virtual DbSet<ProcessingStep> ProcessingStep { get; set; }
        public virtual DbSet<Recipe> Recipe { get; set; }
        public virtual DbSet<RecipeImage> RecipeImage { get; set; }
        public virtual DbSet<RelationshipRecipeAndIngredients> RelationshipRecipeAndIngredients { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Configuration.GetConnString());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ingredients>(entity =>
            {
                entity.HasKey(e => e.IngredientId);

                entity.Property(e=>e.IngredientId).ValueGeneratedOnAdd();

                entity.Property(e => e.IngredientDescription).IsUnicode(false);

                entity.Property(e => e.IngredientName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProcessingStep>(entity =>
            {
                entity.HasKey(e => e.Stepid);

                entity.Property(e => e.Stepid)
                    .HasColumnName("stepid");

                entity.Property(e=>e.Stepid).ValueGeneratedOnAdd();
                entity.Property(e => e.RecipeId).HasColumnName("recipeId");

                entity.Property(e => e.StepDescription)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.StepImage).IsUnicode(false);

                entity.HasOne(d => d.Recipe)
                    .WithMany(p => p.ProcessingStep)
                    .HasForeignKey(d => d.RecipeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProcessingStep_Recipe");
            });

            modelBuilder.Entity<Recipe>(entity =>
            {
                entity.Property(e=>e.RecipeId).ValueGeneratedOnAdd();
                entity.Property(e => e.RecipeDescription).IsUnicode(false);

                entity.Property(e => e.RecipeName)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RecipeImage>(entity =>
            {
                entity.HasKey(e => e.ImageId);

                entity.Property(e=>e.ImageId).ValueGeneratedOnAdd();

                entity.Property(e => e.Image).IsUnicode(false);

                entity.Property(e => e.Recipeid).HasColumnName("recipeid");

                entity.HasOne(d => d.Recipe)
                    .WithMany(p => p.RecipeImage)
                    .HasForeignKey(d => d.Recipeid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RecipeImage_Recipe");
            });

            modelBuilder.Entity<RelationshipRecipeAndIngredients>(entity =>
            {
                entity.ToTable("Relationship_RecipeAndIngredients");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                 entity.Property(e=>e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Ingredientid).HasColumnName("ingredientid");

                entity.Property(e => e.Menge).HasColumnName("menge");

                entity.Property(e => e.Recipeid).HasColumnName("recipeid");

                entity.Property(e => e.Unit)
                    .HasColumnName("unit")
                    .IsUnicode(false);

                entity.HasOne(d => d.Ingredient)
                    .WithMany(p => p.RelationshipRecipeAndIngredients)
                    .HasForeignKey(d => d.Ingredientid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RelationshipBRecipeAndIngredients_Ingredients");

                entity.HasOne(d => d.Recipe)
                    .WithMany(p => p.RelationshipRecipeAndIngredients)
                    .HasForeignKey(d => d.Recipeid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RelationshipBRecipeAndIngredients_Recipe1");
            });
        }
    }
}
