﻿using System;
using System.Collections.Generic;

namespace PinguinKitchen.Model
{
    public partial class RelationshipRecipeAndIngredients
    {
        public int Recipeid { get; set; }
        public int Ingredientid { get; set; }
        public double? Menge { get; set; }
        public string Unit { get; set; }
        public int Id { get; set; }

        public Ingredients Ingredient { get; set; }
        public Recipe Recipe { get; set; }
    }
}
