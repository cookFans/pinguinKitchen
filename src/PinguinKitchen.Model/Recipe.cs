﻿using System;
using System.Collections.Generic;

namespace PinguinKitchen.Model
{
    public partial class Recipe
    {
        public Recipe()
        {
            ProcessingStep = new HashSet<ProcessingStep>();
            RecipeImage = new HashSet<RecipeImage>();
            RelationshipRecipeAndIngredients = new HashSet<RelationshipRecipeAndIngredients>();
        }

        public int RecipeId { get; set; }
        public string RecipeDescription { get; set; }
        public string RecipeName { get; set; }

        public ICollection<ProcessingStep> ProcessingStep { get; set; }
        public ICollection<RecipeImage> RecipeImage { get; set; }
        public ICollection<RelationshipRecipeAndIngredients> RelationshipRecipeAndIngredients { get; set; }
    }
}
