﻿using System;
using System.Collections.Generic;

namespace PinguinKitchen.Model
{
    public partial class RecipeImage
    {
        public int ImageId { get; set; }
        public string Image { get; set; }
        public int Recipeid { get; set; }

        public Recipe Recipe { get; set; }
    }
}
